# HƯỚNG DẪN CHẠY CHƯƠNG TRÌNH ADD-IN

| Members          | ID       |
| ---------------- | -------- |
| Nguyễn Quang Phi | 18120503 |
| Nguyễn Kha Vĩ    | 19120715 |
| Tô Đình Vin      | 19120718 |

### Cài đặt các điều kiện tiên quyết

-   Nodejs: https://nodejs.org/en/
-   Phiên bản mới nhất của Yeoman và trình tạo Yeoman cho Phần bổ trợ Office. Để cài đặt các công cụ này trên toàn cầu, hãy chạy lệnh sau qua dấu nhắc lệnh:
    `npm install -g yo generator-office`
-   Python

### Để chạy chương trình: chạy client và server

-   Chạy client bằng câu lệnh `npm start`
    Allow localhost loopback for Microsoft Edge WebView? (Y/n): Chọn n
    Chương trình sẽ mở phần mềm word và hiển hiện add-in ở góc phải trên màn hình

-   Chạy server bằng câu lệnh : `py index.py` (tùy vào phiên bản python mà sẽ dùng câu lệnh khác nhau, ví dụ: `python index.py` hoặc `python3 index.py`)

### Thống kê văn bản bằng add-ins

-   Chọn Show Taskpane, tiếp theo chọn OK để mở add-in. Lần đẩu mở lên cần chờ 1 đến 2 phút để chương trình hoàn thành việc tải lên
-   Sau khi chương trình đã hoàn thành việc tải lên: Giao diện sẽ có 1 button run
-   Tiến hành nhập văn bản bình thường
-   Nhấn `run`
-   Kết quả thống kê sẽ xuất hiện trong add-in
