import * as React from "react";
import axios from "axios";
import ChartTable from "./ChartTable";
import { DefaultButton } from "@fluentui/react";

const Summary = ({ summary }) => {
  if (!summary) return <></>;
  return (
    <>
      <h2>Summary</h2>
      <div class="content">
        <Item id={1} name="Characters per word" value={summary.characters_per_word} />
        <Item id={2} name="Syllables" value={summary.syllables} />
        <Item id={3} name="Syllables per word" value={summary.syllables_per_word} />
        <Item id={4} name="Total a z character count" value={summary.total_a_z_character_count} />
        <Item id={5} name="Total all character count" value={summary.total_all_character_count} />
        <Item id={6} name="Total sentence count" value={summary.total_sentence_count} />
        <Item id={7} name="Total unique character count" value={summary.total_unique_character_count} />
        <Item id={8} name="Total unique word count" value={summary.total_unique_word_count} />
        <Item id={9} name="Total word count" value={summary.total_word_count} />
        <Item id={10} name="Words per sentence" value={summary.words_per_sentence} />
      </div>
    </>
  );
};

const Readability = ({ readability }) => {
  if (!readability) return <></>;
  return (
    <>
      <h2>Readability</h2>
      <div class="content">
        <Item id={1} name="Characters per word" value={readability.automated_readability_index} />
        <Item id={2} name="Syllables" value={readability.coleman_liau_grade} />
        <Item id={3} name="Syllables per word" value={readability.flesch_kincaid_grade_level} />
        <Item id={4} name="Total a z character count" value={readability.flesch_reading_ease} />
        <Item id={5} name="Total all character count" value={readability.gunning_fog_index} />
        <Item id={6} name="Total sentence count" value={readability.hard_words} />
        <Item id={7} name="Total unique character count" value={readability.laesbarheds_index} />
        <Item id={8} name="Total unique word count" value={readability.lexical_density} />
        <Item id={9} name="Total word count" value={readability.long_words} />
        <Item id={10} name="Words per sentence" value={readability.smog_grade} />
      </div>
    </>
  );
};

const Item = ({ id, name, value }) => {
  return (
    <div
      key={id}
      style={{
        display: "flex",
        justifyContent: "space-between",
        borderTop: "1px solid #E0E0E0",
        borderBottom: "1px solid #E0E0E0",
      }}
    >
      <p style={{ fontWeight: "bold" }}>{name}</p>
      <p>{value.toFixed(3)}</p>
    </div>
  );
};

const Analysis = () => {
  const [showAnalysis, setShowAnalysis] = React.useState(false);
  const [data, setData] = React.useState({});
  const click = async () => {
    return Word.run(async (context) => {
      var documentBody = context.document.body;
      context.load(documentBody);
      await context.sync();

      const newData = await getData(documentBody.text);
      setData(newData);
      setShowAnalysis(true);

      await context.sync();
    });
  };

  const getData = async (content) => {
    const form = new FormData();
    form.append("text", content);
    const data = await axios
      .post("http://127.0.0.1:5000/", form, {
        mode: "no-cors",
        headers: {
          "Content-Type": "multipart/form-data",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers": "*",
          "Access-Control-Allow-Credentials": "true",
        },
      })
      .then((res) => res.data);
    return data;
  };

  if (!showAnalysis) {
    return (
      <>
        <DefaultButton
          style={{
            background: "blue",
            color: "#ffffff",
            fontSize: "18px",
            borderRadius: "4px",
            marginLeft: "50%",
            transform: "translateX(-50%)",
            marginTop: "20px",
          }}
          className="ms-welcome__action"
          iconProps={{ iconName: "ChevronRight" }}
          onClick={click}
        >
          Run
        </DefaultButton>
      </>
    );
  }
  return (
    <>
      <DefaultButton
        style={{
          background: "blue",
          color: "#ffffff",
          fontSize: "18px",
          borderRadius: "4px",
          marginLeft: "50%",
          transform: "translateX(-50%)",
          marginTop: "20px",
        }}
        className="ms-welcome__action"
        iconProps={{ iconName: "ChevronRight" }}
        onClick={click}
      >
        Run
      </DefaultButton>
      <div style={{ padding: "20px" }}>
        <Summary summary={data?.summary} />
        <Readability readability={data?.readability} />
        <ChartTable data={data?.word_lengths} title="Word Length Breakdown" />
        <ChartTable data={data?.pos_analytics.all} title="Parts Of Speech ALL" titleCol="Part" titleRow="Character" />
        <ChartTable
          data={data?.pos_analytics.unique_word}
          title="Parts Of Speech Unique Word"
          titleCol="Part"
          titleRow="Character"
        />
        <ShowEntities data={data} />
      </div>
    </>
  );
};

const ShowEntities = ({ data }) => {
  let keysAll = [];
  let valuesAll = [];
  if (data.entities && data.entities.all) {
    keysAll = Object.keys(data.entities.all);
    valuesAll = Object.values(data.entities.all);
  }

  let keysUnique = [];
  let valuesUnique = [];
  if (data.entities && data.entities.unique_word) {
    keysUnique = Object.keys(data.entities.unique_word);
    valuesUnique = Object.values(data.entities.unique_word);
  }
  return (
    <div>
      <h2>Named entity recognition (ALL) </h2>
      {keysAll?.map((item, idx) => {
        return (
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              borderBottom: "1px solid #E0E0E0",
              padding: "0 16px",
            }}
          >
            <p style={{ fontWeight: "bold" }}>{item}</p>
            <p>{valuesAll[idx]}</p>
          </div>
        );
      })}

      <h2>Named entity recognition (UNIQUE WORD) </h2>
      {keysUnique?.map((item, idx) => {
        return (
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              borderBottom: "1px solid #E0E0E0",
              padding: "0 16px",
            }}
          >
            <p style={{ fontWeight: "bold" }}>{item}</p>
            <p>{valuesUnique[idx]}</p>
          </div>
        );
      })}
    </div>
  );
};

export default Analysis;
