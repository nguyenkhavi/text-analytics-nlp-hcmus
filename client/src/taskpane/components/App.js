import * as React from "react";
import PropTypes from "prop-types";
import Analysis from "./Analysis";

const App = () => {
  return (
    <div className="ms-welcome">
      <Analysis />
    </div>
  );
};

App.propTypes = {
  title: PropTypes.string,
  isOfficeInitialized: PropTypes.bool,
};

export default App;
