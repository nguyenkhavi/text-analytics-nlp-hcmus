import * as React from "react";
import Paper from "@material-ui/core/Paper";
import { Chart, BarSeries, Title, ArgumentAxis, ValueAxis } from "@devexpress/dx-react-chart-material-ui";
import { Animation } from "@devexpress/dx-react-chart";

const ChartTable = ({ data, title, titleCol = "Length", titleRow = "letter words" }) => {
  const array = [];
  let sumWords = 0;
  for (let i in data) {
    array.push({ name: i, value: data[i] });
    sumWords += data[i];
  }
  return (
    <Paper>
      <Chart data={array}>
        <ArgumentAxis />
        <ValueAxis max={7} />
        <BarSeries valueField="value" argumentField="name" />
        <Title text={title} />
        <Animation />
      </Chart>
      <TableAnalysis data={array} sumWords={sumWords} titleCol={titleCol} titleRow={titleRow} />
    </Paper>
  );
};

const TableAnalysis = ({ data, sumWords, titleCol, titleRow }) => {
  return (
    <table class="table">
      <thead>
        <tr>
          <th scope="col">{titleCol}</th>
          <th scope="col">Count</th>
          <th scope="col" style={{ fontSize: "14px" }}>
            Percentage of all {titleRow}
          </th>
        </tr>
      </thead>
      <tbody>
        {data?.map((item, idx) => {
          return (
            <tr key={idx}>
              <td style={{ borderTop: "1px solid #E0E0E0" }}>
                {item.name} {titleRow}{" "}
              </td>
              <td style={{ borderTop: "1px solid #E0E0E0", textAlign: "center" }}>{item.value}</td>
              <td style={{ borderTop: "1px solid #E0E0E0", textAlign: "center" }}>
                {((item.value / sumWords) * 100).toFixed(3)} %
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default ChartTable;
