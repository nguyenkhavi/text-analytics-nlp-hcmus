from flask import Flask, request
from flask_cors import CORS, cross_origin

from core import analysis
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/', methods=['POST'])
@cross_origin()
def main():
    text = request.form.get('text', '')
    result = analysis(text)
    return result


if __name__ == '__main__':
    app.run()
