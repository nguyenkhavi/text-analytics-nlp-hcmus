from underthesea import sent_tokenize, word_tokenize, pos_tag, ner
import string
import re
import math
import utilities as utilities
text = 'Tiến hành thành lập Tổ hỗ trợ theo dõi người nhiễm COVID-19 tại nhà (trước ngày 20/12/2021), với lực lượng thanh niên là nòng cốt và chủ trì triển khai. Nhiệm vụ của Tổ hỗ trợ là thực hiện tiếp nhận thông tin từ người nhiễm COVID-19 tại nhà theo quy định, cập nhật vào hệ thống phần mềm quản lý F0, lấy mẫu xét nghiệm, hướng dẫn các biện pháp thực hiện cách ly tại nhà, ghi chép các thông tin và khi có những dấu hiệu bất thường về sức khỏe của người cách ly thì thông báo ngay cho cán bộ y tế của Trạm y tế.'


def analysis_pos(parts_of_speech):
    pos_analytics = {}
    pos_all = {}
    words = []
    pos_unique_word = {}
    for word, part in parts_of_speech:
        pos_all[part] = 1 if part not in [
            key for key in list(pos_all.keys())] else pos_all[part]+1

        if word not in words:
            pos_unique_word[part] = 1 if part not in [
                key for key in list(pos_unique_word.keys())] else pos_unique_word[part]+1

        words.append(words)

    pos_analytics['all'] = pos_all
    pos_analytics['unique_word'] = pos_unique_word
    return pos_analytics


def analysis_entities(entities):
    entities_analytics = {}
    entities_all = {}
    words = []
    entities_unique_word = {}
    for word, _, _, type in entities:
        entities_all[type] = 1 if type not in [
            key for key in list(entities_all.keys())] else entities_all[type]+1

        if word not in words:
            entities_unique_word[type] = 1 if type not in [
                key for key in list(entities_unique_word.keys())] else entities_unique_word[type]+1

        words.append(words)

    entities_analytics['all'] = entities_all
    entities_analytics['unique_word'] = entities_unique_word
    return entities_analytics


def analysis(text):
    analytics = {}

    # get summary
    summary = {}
    sentences = sent_tokenize(text)

    summary["total_sentence_count"] = len(sentences)

    words = []
    parts_of_speech = []
    entities = []
    for sent in sentences:
        entities.extend(ner(sent))
        words.extend(word_tokenize(sent))
        parts_of_speech.extend(pos_tag(sent))

        # words.extend(word_tokenize(sent))

    summary["total_word_count"] = len(words)
    summary["total_unique_word_count"] = len(utilities.unique(words))
    summary["words_per_sentence"] = len(words) / len(sentences)

    syllables = []
    all_characters = []
    number_characters = []
    long_words = []
    hard_words = []
    word_lengths = {}

    for word in words:
        syllables.append(utilities.get_syllables(word))
        if len(utilities.get_syllables(word)) >= 3:
            # word is a hard word
            hard_words.append(word)

        characters = utilities.get_characters(word)
        all_characters.extend(characters)

        word_lengths[str(len(characters))] = 1 if str(
            len(characters)) not in word_lengths else word_lengths[str(len(characters))] + 1

        if len(characters) > 6:
            # word is a long word
            long_words.append(word)

        numbers = utilities.get_number_characters(word)
        number_characters.extend(list(numbers))

    summary["total_all_character_count"] = len(all_characters)
    summary["total_unique_character_count"] = len(
        utilities.unique(all_characters))
    summary["total_a_z_character_count"] = len(
        all_characters) - len(number_characters)

    summary["characters_per_word"] = len(all_characters) / len(words)
    summary["syllables"] = len(syllables)
    summary["syllables_per_word"] = len(syllables) / len(words)

    # get readability
    readability = {}
    readability['long_words'] = len(long_words)
    readability['hard_words'] = len(hard_words)

    readability['lexical_density'] = 100 * \
        summary["total_unique_word_count"]/summary["total_word_count"]

    readability['gunning_fog_index'] = 0.4*(summary["words_per_sentence"] + (
        100*readability["hard_words"]/summary["total_word_count"]))

    readability['coleman_liau_grade'] = 5.89 * summary["characters_per_word"] - \
        0.3*summary["total_sentence_count"] / \
        (100 * summary["total_word_count"]) - 15.8

    readability['flesch_kincaid_grade_level'] = 0.39 * \
        summary["words_per_sentence"] + 11.8 * \
        summary["syllables_per_word"] - 15.59

    readability['flesch_reading_ease'] = 206.835 - 1.015 * \
        summary["words_per_sentence"] - 84.6*summary["syllables_per_word"]

    readability['automated_readability_index'] = 0.5 * \
        summary["words_per_sentence"] + 4.71 * \
        summary["characters_per_word"] - 21.43

    readability['smog_grade'] = 1.043 * \
        math.sqrt(readability["hard_words"]*30 /
                  summary["total_sentence_count"]) + 3.1291

    readability['laesbarheds_index'] = summary["words_per_sentence"] + \
        100*readability["long_words"]/summary["total_word_count"]
    # get word length
    word_lengths

    # get pos analytics
    pos_analytics = analysis_pos(parts_of_speech)

    # get named entity recognition
    entities = analysis_entities(entities)

    analytics['summary'] = summary
    analytics['readability'] = readability
    analytics['word_lengths'] = word_lengths
    analytics['entities'] = entities
    analytics['pos_analytics'] = pos_analytics

    return analytics


re = analysis(text)
print(re)
