import re


def unique(list1):

    # initialize a null list
    unique_list = []

    # traverse for all elements
    for x in list1:
        # check if exists in unique_list or not
        if x not in unique_list:
            unique_list.append(x)
    return unique_list


def get_characters(word):
    no_space_word = word.replace(' ', '')
    return list(no_space_word)


def get_number_characters(word):
    return re.findall(r"[0-9]", word)


def get_syllables(word):
    return word.split(' ')
